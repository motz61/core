// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.

#include <iostream>
#include <memory>

#include "../core/application.hpp"
#include "application.hpp"

#ifdef SPLIT_CORES
int main() {
    auto app = std::make_shared<core::Application>();
    auto gameApp = std::make_shared<game::Application>(app);
    app->Start(gameApp);

#ifdef _DEBUG
    // Wait for key to exit process when debug executable is compiled
    std::cout << "Press any key to exit the application." << std::endl;
    getchar();
#endif

    return 0;
}
#endif