// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
#pragma once

#include <cstdint>
#include <string>
#include <unordered_map>

namespace game::formats {
    struct Job {
        uint8_t id;
        std::string name;

        uint8_t ht;
        uint8_t st;
        uint8_t dx;
        uint8_t iq;

        uint32_t startHp;
        uint32_t startSp;

        uint16_t hpPerHt;
        uint16_t hpPerLevel;
        uint16_t spPerIq;
        uint16_t spPerLevel;
    };

    class JobManager {
       public:
        JobManager();
        virtual ~JobManager();

        void Load();
        [[nodiscard]] const Job &GetJob(uint8_t jobId) const;

       private:
        std::unordered_map<uint8_t, Job> _jobs;
    };
}  // namespace game::formats
