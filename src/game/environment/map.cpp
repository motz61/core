// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.

#include "map.hpp"

#include <boost/filesystem.hpp>
#include <boost/format.hpp>
#include <boost/format/group.hpp>
#include <utility>

#include "../../core/logger.hpp"
#include "../../core/networking/server.hpp"
#include "../../core/profiler.hpp"
#include "../../core/utils/file.hpp"
#include "../application.hpp"
#include "player.hpp"

namespace game::environment {
    Map::Map(std::string name, uint64_t x, uint64_t y, uint8_t width, uint8_t height, std::shared_ptr<World> world)
        : _name(std::move(name)),
          _x(x),
          _y(y),
          _width(width),
          _height(height),
          _world(std::move(world)),
          _spawnData(),
          _objects(),
          _quadTree(x, y, width * UNIT_SIZE, height * UNIT_SIZE) {
        CORE_LOGGING(trace) << "Creating map " << _name << " at " << x << "," << y << " (" << (int)width << "x"
                            << (int)height << ")";

        _attributes = new uint8_t[width * UNIT_SIZE / 100 * height * UNIT_SIZE / 100];
    }

    Map::~Map() { delete[] _attributes; }

    bool Map::Load() {
        bool ret = true;

        try {
            // Load spawn details
            auto spawnConfig = cpptoml::parse_file("data/maps/" + _name + "/spawn.toml");

            auto spawnArray = spawnConfig->get_table_array("spawn");
            for (const auto &spawn : *spawnArray) {
                auto spawnData = std::make_shared<SpawnData>(shared_from_this());
                spawnData->Load(spawn);

                _spawnData.push_back(std::move(spawnData));
            }
        } catch (const cpptoml::parse_exception &e) {
            CORE_LOGGING(warning) << "Failed to load spawn data for map " << _name << ": " << e.what();
            ret = false;
        }

        // Read map attributes
        for (auto x = 0; x < _width; x++) {
            for (auto y = 0; y < _height; y++) {
                auto id = x * 1000 + y;
                auto folderName = boost::format("%d") % boost::io::group(std::setw(6), std::setfill('0'), id);

                auto attrPath = boost::filesystem::path("data") / "maps" / _name / folderName.str() / "attr.atr";
                if (boost::filesystem::exists(attrPath)) {
                    CORE_LOGGING(trace) << "Load attr map " << attrPath;
                    auto data = core::utils::ReadFile(attrPath.string());

                    auto fourCC = *reinterpret_cast<uint16_t *>(&data[0]);
                    auto width = *reinterpret_cast<uint16_t *>(&data[2]);
                    auto height = *reinterpret_cast<uint16_t *>(&data[4]);

                    if (fourCC != 2634 || width != UNIT_SIZE / 100 || height != UNIT_SIZE / 100) {
                        CORE_LOGGING(error) << "Failed to read attr of map " << _name << " at " << x << "," << y;
                        continue;
                    }

                    for (auto inChunkY = 0; inChunkY < UNIT_SIZE / 100; inChunkY++) {
                        for (auto inChunkX = 0; inChunkX < UNIT_SIZE / 100; inChunkX++) {
                            _attributes[(inChunkY + y * UNIT_SIZE / 100) * _width * UNIT_SIZE / 100 +
                                        (inChunkX + x * UNIT_SIZE / 100)] =
                                data[6 + inChunkY * UNIT_SIZE / 100 + inChunkX];
                        }
                    }
                }
            }
        }

        return ret;
    }

    void Map::InitialSpawn() {
        for (auto &spawn : _spawnData) {
            spawn->Spawn();
        }
    }

    void Map::Update(uint32_t elapsedTime) {
        {
            std::lock_guard<std::mutex> lock(_spawnMutex);

            while (!_spawnQueue.empty()) {
                auto &object = _spawnQueue.front();

                if (!_quadTree.Insert(object)) {
                    CORE_LOGGING(error) << "Failed to insert object into root quad tree!";
                    continue;
                }

                object->OnSpawned();

                _objects[object->GetVID()] = object;

                _spawnQueue.pop();
            }
        }

        {
            std::lock_guard<std::mutex> lock(_despawnMutex);

            while (!_despawnQueue.empty()) {
                auto &object = _despawnQueue.front();

                _quadTree.Remove(object);

                std::vector<std::shared_ptr<Object>> objects;
                _quadTree.QueryObjectsAround(objects, object->GetPositionX(), object->GetPositionY(), 10000);

                for (auto &qobject : objects) {
                    if (qobject->GetObjectType() != PLAYER) continue;

                    std::shared_ptr<Player> player = std::static_pointer_cast<Player>(qobject);
                    auto connection = player->GetConnection();
                    if (connection) object->Remove(connection);
                }

                _objects.erase(object->GetVID());

                _despawnQueue.pop();
            }
        }

        for (const auto &object : _objects) {
            object.second->Update(elapsedTime);

            if (object.second->GetPositionModified()) {
                _quadTree.Remove(object.second);
                _quadTree.Insert(object.second);

                object.second->SetPositionModified(false);
            }
        }
    }

    void Map::SpawnObject(const std::shared_ptr<Object> &object) {
        std::lock_guard<std::mutex> lock(_spawnMutex);

        _spawnQueue.push(object);
    }

    void Map::DespawnObject(const std::shared_ptr<Object> &object) {
        std::lock_guard<std::mutex> lock(_despawnMutex);

        _despawnQueue.push(object);
    }

    void Map::QueryObjectsAround(std::vector<std::shared_ptr<Object>> &out, uint64_t x, uint64_t y, uint32_t radius) {
        _quadTree.QueryObjectsAround(out, x, y, radius);
    }

    std::shared_ptr<Object> Map::GetObject(uint32_t vid) {
        if (_objects.find(vid) == _objects.end()) return nullptr;
        return _objects[vid];
    }

    bool Map::IsAttributeSet(uint64_t x, uint64_t y, uint8_t attr) {
        if (x >= _width * UNIT_SIZE || y >= _height * UNIT_SIZE) {
            assert(false);
            return false;
        }

        uint64_t x1 = x / 100;
        uint64_t y1 = y / 100;

        return (_attributes[y1 * _width * UNIT_SIZE / 100 + x1] & attr) == attr;
    }

    bool Map::IsMovablePosition(uint64_t x, uint64_t y) {
        if (x >= _width * UNIT_SIZE || y >= _height * UNIT_SIZE) {
            return false;
        }

        return !IsAttributeSet(x, y, MapAttribute::MAP_ATTR_BLOCK);
    }

    SpawnData::SpawnData(std::shared_ptr<Map> map)
        : _map(std::move(map)), _type(SpawnData::SPAWN_GROUP), _x(0), _y(0), _range(0), _respawnTime(15) {}

    SpawnData::~SpawnData() = default;

    void SpawnData::Load(const std::shared_ptr<cpptoml::table> &config) {
        auto type = config->get_as<std::string>("type");
        if (type) {
            if (*type == "group")
                _type = SpawnData::SPAWN_GROUP;
            else if (*type == "monster")
                _type = SpawnData::SPAWN_MONSTER;
            else {
                CORE_LOGGING(warning) << "Invalid spawn type " << *type << " using default type";
            }
        }

        auto x = config->get_as<int32_t>("x");
        if (x) _x = *x;

        auto y = config->get_as<int32_t>("y");
        if (y) _y = *y;

        auto range = config->get_as<uint32_t>("range");
        if (range) _range = *range;

        auto respawnTime = config->get_as<uint64_t>("respawnTime");
        if (respawnTime) _respawnTime = *respawnTime;

        switch (_type) {
            case SpawnData::SPAWN_GROUP: {
                auto arr = config->get_array_of<int64_t>("groups");
                for (const auto &id : *arr) {
                    _groups.push_back(id);
                }
                break;
            }
            case SpawnData::SPAWN_MONSTER: {
                auto arr = config->get_array_of<int64_t>("monsters");
                for (const auto &id : *arr) {
                    _monsters.push_back(static_cast<uint32_t>(id));
                }
                break;
            }
        }
    }

    void SpawnData::Spawn() {
        if (_type == SpawnData::SPAWN_MONSTER) {
            if (_monsters.empty()) return;

            auto core = Application::GetInstance()->GetCoreApplication();

            auto idx = core->GetRandomNumber<size_t>(0, _monsters.size() - 1);
            auto monsterId = _monsters[idx];

            auto x = _x + core->GetRandomNumber<int32_t>(-static_cast<int32_t>(_range), _range);
            auto y = _y + core->GetRandomNumber<int32_t>(-static_cast<int32_t>(_range), _range);
            auto rotation = core->GetRandomNumber<short>(0, 360);

            // Check if position is movable
            // todo: max retries?
            while (!_map->IsMovablePosition(x * 100, y * 100)) {
                x = _x + core->GetRandomNumber<int32_t>(-static_cast<int32_t>(_range), _range);
                y = _y + core->GetRandomNumber<int32_t>(-static_cast<int32_t>(_range), _range);
            }

            auto monster = _map->_world->CreateMonster(monsterId, _map->_x + x * 100, _map->_y + y * 100, rotation);
            monster->SetGroup(shared_from_this());
            _map->_world->SpawnObject(monster);
            _instances.emplace(monster->GetVID(), std::move(monster));
        } else if (_type == SpawnData::SPAWN_GROUP) {
            if (_groups.empty()) return;

            auto core = Application::GetInstance()->GetCoreApplication();

            auto idx = core->GetRandomNumber<size_t>(0, _groups.size() - 1);
            auto groupId = _groups[idx];

            auto group = _map->_world->GetSpawnGroup(groupId);

            auto x = _x + core->GetRandomNumber<int32_t>(-static_cast<int32_t>(_range), _range);
            auto y = _y + core->GetRandomNumber<int32_t>(-static_cast<int32_t>(_range), _range);
            while (!_map->IsMovablePosition(x * 100, y * 100)) {
                x = _x + core->GetRandomNumber<int32_t>(-static_cast<int32_t>(_range), _range);
                y = _y + core->GetRandomNumber<int32_t>(-static_cast<int32_t>(_range), _range);
            }

            for (const auto &monsterId : group.members) {
                auto rotation = core->GetRandomNumber<short>(0, 360);

                auto monsterX = (x + core->GetRandomNumber<int32_t>(-10, 10)) * 100;
                auto monsterY = (y + core->GetRandomNumber<int32_t>(-10, 10)) * 100;
                // Check if position is movable
                // todo: max retries?
                while (!_map->IsMovablePosition(monsterX, monsterY)) {
                    monsterX = (x + core->GetRandomNumber<int32_t>(-10, 10)) * 100;
                    monsterY = (y + core->GetRandomNumber<int32_t>(-10, 10)) * 100;
                }

                auto monster =
                    _map->_world->CreateMonster(monsterId, _map->_x + monsterX, _map->_y + monsterY, rotation);
                monster->SetGroup(shared_from_this());
                _map->_world->SpawnObject(monster);
                _instances.emplace(monster->GetVID(), std::move(monster));
            }
        }
    }

    void SpawnData::TriggerAll(const std::shared_ptr<Object> &object) const {
        for (const auto &entry : _instances) {
            auto monster = entry.second.lock();
            if (monster) {
                monster->TakeDamage(object, 0);
            }
        }
    }

    void SpawnData::RemoveFromGroup(const std::shared_ptr<Monster> &monster) {
        _instances.erase(monster->GetVID());

        if (_instances.empty()) {
            // Group is empty now, start respawn event
            Application::GetInstance()->GetCoreApplication()->GetEventSystem()->EnqueueEvent(
                [this]() {
                    Spawn();
                    return 0;
                },
                _respawnTime * 1000);
        }
    }
}  // namespace game::environment