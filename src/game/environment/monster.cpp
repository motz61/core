// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.

#include "monster.hpp"

#include "../application.hpp"
#include "ai/simple_behaviour.hpp"

namespace game::environment {

    Monster::Monster(uint32_t id, uint32_t vid, int32_t x, int32_t y, float rotation)
        : Object(vid, x, y, rotation),
          _dead(false),
          _deadTime(5000),
          _spawnX(x),
          _spawnY(y),
          _proto(game::Application::GetInstance()->GetMonster(id)) {
        _hp = _proto.hp;
    }

    Monster::~Monster() {
        CORE_LOGGING(trace) << "Destroy monster " << _proto.translatedName;
        ClearTarget();
    }

    void Monster::Show(const std::shared_ptr<core::networking::Connection>& connection) {
        if (_dead) return;  // we don't advertise if we are dead

        auto monster = connection->GetServer()->GetPacketManager()->CreatePacket(0x01, core::networking::Outgoing);
        monster->SetField<uint32_t>("vid", _vid);
        monster->SetField<uint8_t>("characterType", GetObjectType());
        monster->SetField<float>("angle", GetRotation());
        monster->SetField<int32_t>("x", GetPositionX());
        monster->SetField<int32_t>("y", GetPositionY());
        monster->SetField<uint16_t>("class", _proto.id);
        monster->SetField<uint8_t>("moveSpeed", _proto.moveSpeed);
        monster->SetField<uint8_t>("attackSpeed", _proto.attackSpeed);
        connection->Send(monster);
    }

    void Monster::Remove(const std::shared_ptr<core::networking::Connection>& connection) {
        auto remove =
            connection->GetServer()->GetPacketManager()->CreatePacket(0x02, core::networking::Direction::Outgoing);
        remove->SetField("vid", GetVID());
        connection->Send(remove);
    }

    void Monster::Update(uint32_t elapsedTime) {
        if (!_dead && _behaviour) _behaviour->Update(elapsedTime);

        if (_dead) {
            _deadTime -= elapsedTime;
            if (_deadTime <= 0) {
                GetMap()->DespawnObject(shared_from_this());
            }
        }

        Object::Update(elapsedTime);
    }

    void Monster::OnSpawned() {
        _behaviour = std::make_unique<ai::SimpleBehaviour>(Monster::shared_from_this());
        _behaviour->Init();
    }

    void Monster::ObjectEnteredView(std::shared_ptr<Object> object) {
        if (_behaviour) _behaviour->ObjectEnteredView(object);
    }

    void Monster::ObjectLeftView(std::shared_ptr<Object> object) {}

    void Monster::TakeDamage(const std::shared_ptr<Object>& object, int damage) {
        // Calculate defense value
        uint32_t defense = _proto.level + _proto.ht + _proto.defence;

        if (damage > 0) {
            auto group = _group.lock();
            if (group) group->TriggerAll(object);
        }

        int finalDamage = damage - static_cast<int>(defense);
        if (finalDamage < 0) finalDamage = 0;

        if (_behaviour) {
            _behaviour->TookDamage(object, finalDamage);
        }

        _hp -= finalDamage;

        if (object->GetObjectType() == PLAYER) {
            auto player = std::static_pointer_cast<Player>(object);
            auto connection = player->GetConnection();
            if (!connection) return;

            auto info = connection->GetServer()->GetPacketManager()->CreatePacket(0x87);
            info->SetField<uint32_t>("vid", GetVID());
            info->SetField<uint8_t>("damageType", 1);
            info->SetField<int32_t>("damage", finalDamage);
            connection->Send(info);
        }

        RefreshTarget();

        if (_hp <= 0) {
            Dead();
        }
    }

    void Monster::Dead() {
        if (_dead) return;

        auto group = _group.lock();
        if (group) {
            group->RemoveFromGroup(shared_from_this());
        }

        _dead = true;

        auto dead = Application::GetInstance()->GetServer()->GetPacketManager()->CreatePacket(0x0e);
        dead->SetField<uint32_t>("vid", GetVID());
        SendPacketAround(dead);
    }

    int32_t Monster::GetHP() { return _hp; }
    int32_t Monster::GetMaxHP() { return _proto.hp; }
}  // namespace game::environment