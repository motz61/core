// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
#pragma once

#include <memory>
#include <shared_mutex>

#include "../monster.hpp"
#include "behaviour.hpp"

namespace game::environment::ai {
    enum State { IDLE, MOVING };

    class SimpleBehaviour : public Behaviour {
       public:
        explicit SimpleBehaviour(std::shared_ptr<Monster> &&monster);
        virtual ~SimpleBehaviour();

        void Init() override;
        void Update(uint32_t elapsedTime) override;
        void ObjectEnteredView(const std::shared_ptr<Object> &object) override;
        void TookDamage(const std::shared_ptr<Object> &attacker,
                        uint32_t damage) override;

       private:
        void CalculateNextMovement();
        bool RandomTarget(const std::shared_ptr<Monster> &monster);
        void SendMovePacket(const std::shared_ptr<Monster> &monster);

        bool PickTarget(const std::shared_ptr<Monster> &monster);
        void Goto(const std::shared_ptr<Monster> &monster, int32_t x,
                  int32_t y);
        void Attack(const std::shared_ptr<Monster> &monster,
                    const std::shared_ptr<Object> &target);

       private:
        std::weak_ptr<Monster> _monster;
        State _state = IDLE;

        std::weak_ptr<Object> _currentTarget;
        std::unordered_map<uint32_t, uint64_t> _damageDealt;
        std::shared_mutex _targetMutex;

        int32_t _startX;
        int32_t _startY;
        int32_t _targetX;
        int32_t _targetY;
        int32_t _lastAttackX;
        int32_t _lastAttackY;
        uint32_t _movementStart;
        uint32_t _movementDuration;

        int _nextMovementIn;
        uint32_t _lastAttack;
    };
}  // namespace game::environment::ai
