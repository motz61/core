// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
#include "simple_behaviour.hpp"

#include <utility>

#include "../../../core/utils/math.hpp"
#include "../../application.hpp"

#define ALLOWED_DISTANCE_FROM_LAST_ATTACK 5000

namespace game::environment::ai {
    SimpleBehaviour::SimpleBehaviour(std::shared_ptr<Monster> &&monster)
        : _monster(std::move(monster)),
          _startX(0),
          _startY(0),
          _targetX(0),
          _targetY(0),
          _lastAttackX(0),
          _lastAttackY(0),
          _movementStart(0),
          _movementDuration(0),
          _nextMovementIn(INT_MAX),
          _lastAttack(0) {}

    SimpleBehaviour::~SimpleBehaviour() = default;

    void SimpleBehaviour::Init() { CalculateNextMovement(); }

    void SimpleBehaviour::Update(uint32_t elapsedTime) {
        auto game = Application::GetInstance();
        auto time = game->GetCoreApplication()->GetCoreTime();

        auto monster = _monster.lock();
        if (!monster) {
            CORE_LOGGING(error) << "Stale reference to behavior without monster!";
            assert(false);
            return;
        }

        bool noTarget = true;
        bool pickNewTarget = false;
        {
            std::shared_lock lock(_targetMutex);
            auto target = _currentTarget.lock();
            if (target && target->IsDead()) {
                target = nullptr;
                _currentTarget.reset();
            }

            if (target) {
                noTarget = false;

                // Check if target is in attack range
                float currentDistance = core::utils::Math::Distance(
                    monster->GetPositionX(), monster->GetPositionY(), target->GetPositionX(), target->GetPositionY());
                float targetDistance =
                    core::utils::Math::Distance(_targetX, _targetY, target->GetPositionX(), target->GetPositionY());

                // Check if target is still in range of the monster
                if (monster->GetProto().attackRange < std::abs(std::min(currentDistance, targetDistance))) {
                    float distanceFromLastAttack = core::utils::Math::Distance(
                        _lastAttackX, _lastAttackY, target->GetPositionX(), target->GetPositionY());

                    if (distanceFromLastAttack > ALLOWED_DISTANCE_FROM_LAST_ATTACK) {
                        // Current target is too far away, pick new target
                        pickNewTarget = true;
                    } else {
                        // Move to target
                        Goto(monster, target->GetPositionX(), target->GetPositionY());
                        SendMovePacket(monster);
                        _state = MOVING;

                        CORE_LOGGING(trace) << "Move to my target " << target->GetName();
                    }
                } else if (_lastAttack + 2000 <= time && monster->GetProto().attackRange > std::abs(currentDistance)) {
                    // We are in attack range & attack cooldown is done
                    Attack(monster, target);
                }
            }
        }

        if (pickNewTarget) {
            noTarget = !PickTarget(monster);
            if (noTarget) {
                _nextMovementIn = 0;  // force to go back close to spawn position
            }
        }

        // If we are idling and have no attack target
        if (_state == IDLE && noTarget) {
            _nextMovementIn -= elapsedTime;
            if (_nextMovementIn <= 0) {
                // Calculate move target
                if (RandomTarget(monster)) {
                    // Enable moving state
                    _state = MOVING;

                    // Send "WAIT" packet to players
                    SendMovePacket(monster);

                    CalculateNextMovement();
                }
            }
        }

        // Update player position in moving state
        if (_state == MOVING) {
            auto elapsed = time - _movementStart;
            auto rate = elapsed / (float)_movementDuration;
            if (rate > 1) rate = 1;

            auto x = (int32_t)((float)(_targetX - _startX) * rate + _startX);
            auto y = (int32_t)((float)(_targetY - _startY) * rate + _startY);

            monster->SetPositionX(x);
            monster->SetPositionY(y);

            if (rate >= 1) {
                _state = IDLE;
            }
        }
    }

    void SimpleBehaviour::CalculateNextMovement() {
        _nextMovementIn =
            Application::GetInstance()->GetCoreApplication()->GetRandomNumber<unsigned short>(10000, 20000);
    }

    bool SimpleBehaviour::RandomTarget(const std::shared_ptr<Monster> &monster) {
        const uint16_t moveRadius = 1000;

        auto core = Application::GetInstance()->GetCoreApplication();

        // Choose random location around spawn position
        auto offsetX = core->GetRandomNumber<short>(-moveRadius, moveRadius);
        auto offsetY = core->GetRandomNumber<short>(-moveRadius, moveRadius);
        auto targetX = monster->GetSpawnX() + offsetX;
        auto targetY = monster->GetSpawnY() + offsetY;

        // Verify location is movable
        // todo max retries
        while (!monster->GetMap()->IsMovablePosition(targetX - monster->GetMap()->GetX(),
                                                     targetY - monster->GetMap()->GetY())) {
            offsetX = core->GetRandomNumber<short>(-moveRadius, moveRadius);
            offsetY = core->GetRandomNumber<short>(-moveRadius, moveRadius);
            targetX = monster->GetSpawnX() + offsetX;
            targetY = monster->GetSpawnY() + offsetY;
        }

        Goto(monster, targetX, targetY);

        return true;
    }

    void SimpleBehaviour::SendMovePacket(const std::shared_ptr<Monster> &monster) {
        auto movePacket = Application::GetInstance()->GetServer()->GetPacketManager()->CreatePacket(0x03);
        movePacket->SetField<uint8_t>("movementType", MovementTypes::WAIT);
        movePacket->SetField<uint8_t>("rotation", static_cast<uint8_t>(monster->GetRotation() / 5));
        movePacket->SetField<uint32_t>("vid", monster->GetVID());
        movePacket->SetField<int32_t>("x", _targetX);
        movePacket->SetField<int32_t>("y", _targetY);
        movePacket->SetField<uint32_t>("time", Application::GetInstance()->GetCoreApplication()->GetCoreTime());
        movePacket->SetField<uint32_t>("duration", _movementDuration);

        monster->SendPacketAround(movePacket);
    }

    void SimpleBehaviour::ObjectEnteredView(const std::shared_ptr<Object> &object) {
        if (object->GetObjectType() != PLAYER) return;
        if (_state != MOVING) return;

        auto monster = _monster.lock();
        if (!monster) return;

        auto player = std::static_pointer_cast<Player>(object);

        auto movePacket = Application::GetInstance()->GetServer()->GetPacketManager()->CreatePacket(0x03);
        movePacket->SetField<uint8_t>("movementType", MovementTypes::WAIT);
        movePacket->SetField<uint8_t>("rotation", static_cast<uint8_t>(monster->GetRotation() / 5));
        movePacket->SetField<uint32_t>("vid", monster->GetVID());
        movePacket->SetField<int32_t>("x", _targetX);
        movePacket->SetField<int32_t>("y", _targetY);
        movePacket->SetField<uint32_t>("time", Application::GetInstance()->GetCoreApplication()->GetCoreTime());
        movePacket->SetField<uint32_t>("duration", _movementDuration);  // is ignored by the server

        player->GetConnection()->Send(movePacket);
    }

    void SimpleBehaviour::Goto(const std::shared_ptr<Monster> &monster, int32_t x, int32_t y) {
        auto game = Application::GetInstance();
        auto core = game->GetCoreApplication();

        // Get animation data for run
        auto animation = game->GetAnimationManager()->GetAnimation(
            monster->GetProto().id, formats::AnimationType::RUN, formats::AnimationSubType::GENERAL);
        if (!animation) {
            CORE_LOGGING(warning) << "Can't move monster, no run animation found " << monster->GetProto().id;
            return;
        }

        // Set current position and store target position
        _startX = monster->GetPositionX();
        _startY = monster->GetPositionY();
        _targetX = x;
        _targetY = y;

        // Calculate rotation to target
        auto rotation = core::utils::Math::CalculateRotation(static_cast<float>(_targetX - _startX),
                                                             static_cast<float>(_targetY - _startY));
        if (rotation < 0) {
            rotation += 360;
        }
        monster->SetRotation(rotation);

        // Calculate movement duration
        float distance = core::utils::Math::Distance(_startX, _startY, _targetX, _targetY);
        float animationSpeed = -animation->accumulationY / animation->motionDuration;

        int i = 100 - monster->GetProto().moveSpeed;
        if (i > 0) {
            i = 100 + i;
        } else if (i < 0) {
            i = 10000 / (100 - i);
        } else {
            i = 100;
        }
        int duration = static_cast<int>((distance / animationSpeed) * 1000) * i / 100;

        _movementStart = core->GetCoreTime();
        _movementDuration = duration;
    }

    void SimpleBehaviour::TookDamage(const std::shared_ptr<Object> &attacker, uint32_t damage) {
        if (attacker->GetObjectType() != ObjectType::PLAYER) return;

        // Require unique lock
        std::unique_lock lock(_targetMutex);

        uint64_t total;

        // Update damage dealt by this attacker
        if (_damageDealt.find(attacker->GetVID()) == _damageDealt.end()) {
            total = damage;
        } else {
            total = _damageDealt[attacker->GetVID()] + damage;
        }
        _damageDealt[attacker->GetVID()] = total;

        auto currentTarget = _currentTarget.lock();
        if (!currentTarget) {
            // The current target is empty or invalid
            _currentTarget = attacker;
            CORE_LOGGING(trace) << "Set target to " << attacker->GetName();
            _lastAttackX = attacker->GetPositionX();
            _lastAttackY = attacker->GetPositionY();
            return;
        }

        if (currentTarget == attacker) {
            // Current target is the attacker, target can't have changed
            _lastAttackX = attacker->GetPositionX();
            _lastAttackY = attacker->GetPositionY();
            return;
        }

        auto currentTargetTotalDamage = _damageDealt[currentTarget->GetVID()];
        if (currentTargetTotalDamage < total) {
            // The attacker now dealt more damage, change our target
            _currentTarget = attacker;
            CORE_LOGGING(trace) << "Set target to " << attacker->GetName();
            _lastAttackX = attacker->GetPositionX();
            _lastAttackY = attacker->GetPositionY();
        }
    }

    bool SimpleBehaviour::PickTarget(const std::shared_ptr<Monster> &monster) {
        std::unique_lock lock(_targetMutex);

        CORE_LOGGING(trace) << "Pick new target";

        std::shared_ptr<Object> newTarget;
        uint64_t currentMaxDamage = 0;
        for (auto it = _damageDealt.begin(); it != _damageDealt.end();) {
            auto obj = monster->GetMap()->GetObject(it->first);
            if (!obj) {
                it = _damageDealt.erase(it);
                continue;
            }

            if (it->second >= currentMaxDamage) {
                float distance =
                    core::utils::Math::Distance(_lastAttackX, _lastAttackY, obj->GetPositionX(), obj->GetPositionY());

                if (std::abs(distance) <= ALLOWED_DISTANCE_FROM_LAST_ATTACK) {
                    newTarget = obj;
                    currentMaxDamage = it->second;
                } else {
                    // target is too far away
                }
            }

            it++;
        }

        if (newTarget) {
            CORE_LOGGING(trace) << "New target is " << newTarget->GetName();
        }

        _currentTarget = newTarget;
        return newTarget != nullptr;
    }

    void SimpleBehaviour::Attack(const std::shared_ptr<Monster> &monster, const std::shared_ptr<Object> &target) {
        auto game = Application::GetInstance();
        auto time = game->GetCoreApplication()->GetCoreTime();

        // Calculate rotation to our target
        auto rotation =
            core::utils::Math::CalculateRotation(static_cast<float>(target->GetPositionX() - monster->GetPositionX()),
                                                 static_cast<float>(target->GetPositionY() - monster->GetPositionY()));

        // Set rotation
        monster->SetRotation(rotation);

        // Send attack packet
        auto attack = game->GetServer()->GetPacketManager()->CreatePacket(0x03);
        attack->SetField<uint8_t>("movementType", MovementTypes::ATTACK);
        attack->SetField<uint8_t>("rotation", monster->GetRotation() / 5);
        attack->SetField<uint32_t>("vid", monster->GetVID());
        attack->SetField<int32_t>("x", monster->GetPositionX());
        attack->SetField<int32_t>("y", monster->GetPositionY());
        attack->SetField<uint32_t>("time", time);
        monster->SendPacketAround(attack);

        // Store this time to calculate next attack time
        _lastAttack = time;

        // Calculate attack damage
        auto minDamage = monster->GetProto().damageRange[0];
        auto maxDamage = monster->GetProto().damageRange[1];
        auto damage = game->GetCoreApplication()->GetRandomNumber(minDamage, maxDamage) * 2;

        target->TakeDamage(monster, damage);
    }
}  // namespace game::environment::ai