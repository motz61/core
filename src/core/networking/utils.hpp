// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.

#pragma once

#include <string>

namespace core::networking {
    std::string GetLocalAddress();
    int32_t ConvertIP(const std::string &ip);
}  // namespace core::networking