// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.

#pragma once

#include <cstdint>
#include <memory>
#include <random>
#include <vector>

#include "../mysql/mysql.hpp"
#include "redis.hpp"

namespace core::cache {
    struct Player {
        uint32_t id;
        std::string name;
        uint8_t playerClass;
        uint8_t skillGroup;
        uint32_t playtime;
        uint8_t level;
        uint32_t exp;
        uint32_t gold;
        uint8_t st;
        uint8_t ht;
        uint8_t dx;
        uint8_t iq;
        int32_t posX;
        int32_t posY;
        int32_t teleportationX;
        int32_t teleportationY;

        int64_t hp;
        int64_t mp;
        int64_t stamina;
    };

    class PlayerCache {
       public:
        PlayerCache(std::shared_ptr<Redis> redis, std::shared_ptr<mysql::MySQL> database);
        virtual ~PlayerCache();

        // todo: move to account cache!
        uint32_t GenerateLoginKey(uint32_t accountId);
        uint32_t CheckLoginKey(uint32_t loginKey);
        void RemoveLoginKeyTTL(uint32_t loginKey);
        void SetLoginKeyTTL(uint32_t loginKey);

        void GetPlayer(uint32_t playerId, const std::function<void(Player)> &cb);
        void SetTeleportationPosition(uint32_t playerId, int32_t x, int32_t y);
        void SavePlayer(const Player &player);
        void GetPlayers(uint32_t accountId, const std::function<void(const std::vector<uint32_t> &)> &cb);
        void AddPlayer(uint32_t accountId, uint32_t playerId, uint8_t slot);

       private:
        std::shared_ptr<Redis> _redis;
        std::shared_ptr<mysql::MySQL> _database;

        std::random_device _randomDevice;
        std::mt19937 _random;
    };
}  // namespace core::cache
