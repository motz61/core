// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.

#include "migration_manager.hpp"

#include <algorithm>

#include "../logger.hpp"
#include "migrations/migrations.h"

namespace core::mysql {
    void MigrationManager::AddMigration(Migration *migration) { _migrations.push_back(migration); }

    void MigrationManager::LoadMigrations() {
        AddMigration(new migrations::M20181013_234200_CreateAccounts());
        AddMigration(new migrations::M20181027_214100_CreatePlayers());
        AddMigration(new migrations::M20181027_224500_CreatePlayerData());
        AddMigration(new migrations::M20181027_225700_CreatePlayerEquipment());
        AddMigration(new migrations::M20181027_225900_CreatePlayerSkills());
        AddMigration(new migrations::M20181027_230100_CreatePlayerQuickSlots());
    }

    MigrationManager::MigrationManager() : _migrations() {}

    MigrationManager::~MigrationManager() {
        for (auto migration : _migrations) {
            delete migration;
        }
    }

    bool MigrationManager::Migrate(std::shared_ptr<MySQL> mysql) {
        // ensure all databases exists
        mysql->ExecuteSync("CREATE DATABASE IF NOT EXISTS `account`");
        mysql->ExecuteSync("CREATE DATABASE IF NOT EXISTS `game`");

        // ensure that migrations table exists
        try {
            mysql->ExecuteSync(
                "CREATE TABLE IF NOT EXISTS `account`.`migrations`(`name` "
                "varchar(255), primary key(`name`)) ENGINE=InnoDB DEFAULT "
                "CHARSET=utf8mb4;");
        } catch (AMY_SYSTEM_NS::system_error const &e) {
            CORE_LOGGING(fatal) << "Failed to create migrations table!";
            CORE_LOGGING(fatal) << e.what();
            return false;
        } catch (...) {
            CORE_LOGGING(fatal) << "Failed to create migration table!";
            return false;
        }

        // sort migrations ascending
        std::sort(_migrations.begin(), _migrations.end(), [](Migration *left, Migration *right) {
            return left->GetName() < right->GetName();
        });

        for (auto migration : _migrations) {
            CORE_LOGGING(trace) << "Checking migration " << migration->GetName();
            auto count = stoi(mysql->ExecuteScalarSync("SELECT COUNT(*) FROM `account`.`migrations` WHERE `name`='" +
                                                       migration->GetName() + "'"));
            if (count == 1) {
                CORE_LOGGING(trace) << "Already done..";
                continue;
            }

            if (migration->Execute(mysql)) {
                CORE_LOGGING(trace) << "Migrated.";

                try {
                    mysql->ExecuteSync("INSERT INTO `account`.`migrations`(`name`) VALUES('" + migration->GetName() +
                                       "')");
                } catch (AMY_SYSTEM_NS::system_error const &e) {
                    CORE_LOGGING(fatal) << "Failed to mark migration as done!";
                    CORE_LOGGING(fatal) << e.what();
                    return false;
                } catch (...) {
                    CORE_LOGGING(fatal) << "Failed to mark migration as done!";
                    return false;
                }
            } else {
                return false;
            }
        }

        return false;
    }
}  // namespace core::mysql