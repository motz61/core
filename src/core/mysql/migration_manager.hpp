// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.

#pragma once

#include <memory>
#include <vector>

#include "migration.hpp"
#include "mysql.hpp"

namespace core::mysql {
    class MigrationManager {
       public:
        MigrationManager();
        virtual ~MigrationManager();

        bool Migrate(std::shared_ptr<MySQL> mysql);
        void AddMigration(Migration *name);

        void LoadMigrations();

       private:
        std::vector<Migration *> _migrations;
    };
}  // namespace core::mysql