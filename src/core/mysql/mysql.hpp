// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.

#pragma once

#include <amy/connector.hpp>
#include <atomic>
#include <boost/asio/deadline_timer.hpp>
#include <string>

#include "result_set.hpp"
#include "statement.hpp"

namespace core::mysql {
    class MigrationManager;

    class MySQLError {
       public:
        explicit MySQLError(const boost::system::error_code &error);
        MySQLError();

        operator bool() const { return _error.failed(); }
        std::string message() const { return _error.message(); }

       private:
        boost::system::error_code _error;
    };

    class MySQL : public std::enable_shared_from_this<MySQL> {
        friend class ResultSet;

        friend class Statement;

       public:
        MySQL(boost::asio::io_service &service);
        virtual ~MySQL();

        void Connect(const std::string &host, const std::string &username, const std::string &password,
                     unsigned int port);
        void Reconnect(unsigned int seconds = 0);

        std::string GetError(AMY_SYSTEM_NS::error_code &ec);

        std::string EscapeString(const std::string &str);

        void EnableAutocommit();
        void DisableAutocommit();
        boost::system::error_code Commit();
        boost::system::error_code Rollback();

        Statement CreateStatement(const std::string &statement);

        void ExecuteQuery(const std::string &statement,
                          const std::function<void(const MySQLError &error, std::shared_ptr<ResultSet>)> &cb);
        void ExecuteMultiQuery(
            const std::string &statement,
            const std::function<void(const MySQLError &error, std::shared_ptr<std::vector<std::shared_ptr<ResultSet>>>)>
                cb);

        std::shared_ptr<ResultSet> ExecuteQuerySync(const std::string &statement);

        void Execute(const std::string &statement);
        void ExecuteSync(const std::string &statement);

        void ExecuteScalar(const std::string &statement,
                           const std::function<void(const MySQLError &error, std::string)> &cb);
        std::string ExecuteScalarSync(const std::string &statement);

       protected:
        amy::connector _connector;

       private:
        boost::asio::io_service &_service;
        boost::asio::deadline_timer _reconnectTimer;
        boost::asio::ip::tcp::resolver _resolver;

        std::string _host = "";
        std::string _username = "";
        std::string _password = "";
        unsigned int _port = 3306;
        std::atomic<bool> _isReconnecting = false;
    };
}  // namespace core::mysql