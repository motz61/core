// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.

#include "statement.hpp"

#include <future>
#include <mysql.h>
#include <utility>

#include "mysql.hpp"

namespace core::mysql {
    Statement::Statement(std::shared_ptr<MySQL> mysql, const std::string &statement)
        : _mysql(std::move(mysql)), _statement(statement), _formatter(statement) {}

    std::string Statement::EscapeString(const std::string &str) { return _mysql->EscapeString(str); }

    std::shared_ptr<ResultSet> Statement::operator()() { return ExecuteSync(); }

    void Statement::Execute(const std::function<void(const MySQLError &error, std::shared_ptr<ResultSet>)> &cb) {
        _mysql->ExecuteQuery(GetStatement(), cb);
    }

    void Statement::ExecuteMultiStatement(
        const std::function<void(const MySQLError &, std::shared_ptr<std::vector<std::shared_ptr<ResultSet>>>)> &cb) {
        _mysql->ExecuteMultiQuery(GetStatement(), cb);
    }

    std::shared_ptr<ResultSet> Statement::ExecuteSync() { return _mysql->ExecuteQuerySync(GetStatement()); }

    std::string Statement::GetStatement() { return _formatter.str(); }

    Statement &operator<<(Statement &stmt, const std::string &value) {
        stmt._formatter % ("'" + stmt.EscapeString(value) + "'");
        return stmt;
    }

    Statement &operator<<(Statement &stmt, int value) {
        stmt._formatter % value;
        return stmt;
    }
}  // namespace core::mysql