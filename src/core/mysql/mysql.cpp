// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.

#include "mysql.hpp"

#include <amy/placeholders.hpp>
#include <boost/asio/ip/tcp.hpp>

#include "../logger.hpp"

namespace asio = boost::asio;
using tcp = boost::asio::ip::tcp;

namespace core::mysql {
    MySQL::MySQL(asio::io_service &service)
        : _connector(service), _service(service), _reconnectTimer(service), _resolver(service) {}

    MySQL::~MySQL() {}

    std::string MySQL::GetError(AMY_SYSTEM_NS::error_code &ec) { return _connector.error_message(ec); }

    void MySQL::Connect(const std::string &host, const std::string &username, const std::string &password,
                        unsigned int port) {
        _host = host;
        _username = username;
        _password = password;
        _port = port;

        auto endpoints = _resolver.resolve(_host, std::to_string(_port));

        _connector.connect(endpoints.begin()->endpoint(),
                           amy::auth_info(username, password),
                           "",
                           amy::detail::client_compress | amy::detail::client_multi_statements);
    }

    void MySQL::Reconnect(unsigned int seconds) {
        if (_isReconnecting) {
            return;
        }

        if (seconds > 0) {
            _isReconnecting = true;
            _reconnectTimer.expires_from_now(boost::posix_time::seconds(seconds));
            _reconnectTimer.async_wait([this](auto err) {
                if (err) {
                    CORE_LOGGING(error) << "Timer failed: " << err.message();
                }

                _isReconnecting = false;
                Reconnect();
            });

            return;
        }

        _isReconnecting = true;

        CORE_LOGGING(error) << "Connection to mysql lost, reconnecting";

        _resolver.async_resolve(_host, std::to_string(_port), [this](auto err, auto results) {
            if (err) {
                CORE_LOGGING(error) << "Failed to resolve database, retry in 5 seconds: " << err.message();
                _isReconnecting = false;
                Reconnect(5);
                return;
            }

            _connector.async_connect(results.begin()->endpoint(),
                                     amy::auth_info(_username, _password),
                                     "",
                                     amy::detail::client_compress | amy::detail::client_multi_statements,
                                     [this](auto error) {
                                         if (error) {
                                             _isReconnecting = false;
                                             CORE_LOGGING(error) << "Failed to reconnect to mysql, retry in 5 seconds";
                                             Reconnect(5);
                                             return;
                                         }

                                         _isReconnecting = false;
                                         CORE_LOGGING(info) << "Connection to mysql restored!";
                                     });
        });
    }

    void MySQL::EnableAutocommit() { _connector.autocommit(true); }

    void MySQL::DisableAutocommit() { _connector.autocommit(false); }

    std::string MySQL::EscapeString(const std::string &str) {
        auto buffer =
            new char[str.size() * 2 + 1];  // buffer size is recommended
                                           // (https://dev.mysql.com/doc/refman/8.0/en/mysql-real-escape-string.html)
        auto len =
            mysql_real_escape_string(_connector.native(), buffer, str.c_str(), static_cast<unsigned long>(str.size()));
        auto ret = std::string(buffer, len);
        delete[] buffer;

        return ret;
    }

    boost::system::error_code MySQL::Commit() {
        boost::system::error_code ec;
        return _connector.commit(ec);
    }

    boost::system::error_code MySQL::Rollback() {
        boost::system::error_code ec;
        return _connector.rollback(ec);
    }

    Statement MySQL::CreateStatement(const std::string &statement) { return Statement(shared_from_this(), statement); }

    void MySQL::ExecuteQuery(const std::string &statement,
                             const std::function<void(const MySQLError &error, std::shared_ptr<ResultSet>)> &cb) {
        _connector.async_query_result(statement, [this, cb](boost::system::error_code error, auto resultSet) {
            if (error) {
                if (error == amy::error::server_gone_error || error == amy::error::server_lost) {
                    // We have lost our connection
                    Reconnect();
                }

                cb(MySQLError(error), nullptr);
                return;
            }

            cb({}, std::make_shared<ResultSet>(resultSet));
        });
    }

    void MySQL::ExecuteMultiQuery(
        const std::string &statement,
        const std::function<void(const MySQLError &error, std::shared_ptr<std::vector<std::shared_ptr<ResultSet>>>)>
            cb) {
        _connector.async_query(statement, [this, cb](boost::system::error_code error) {
            if (error) {
                if (error == amy::error::server_gone_error || error == amy::error::server_lost) {
                    // We have lost our connection
                    Reconnect();
                }

                cb(MySQLError(error), {});
                return;
            }

            auto ret = std::make_shared<std::vector<std::shared_ptr<ResultSet>>>();
            auto callback = [this, cb, ret](
                                auto callback, boost::system::error_code error, amy::result_set result) -> void {
                ret->push_back(std::make_shared<ResultSet>(result));

                if (_connector.has_more_results()) {
                    _connector.async_store_result([callback](boost::system::error_code error, amy::result_set result) {
                        callback(callback, error, result);
                    });
                } else {
                    cb({}, ret);
                }
            };

            _connector.async_store_result([callback](boost::system::error_code error, amy::result_set result) {
                callback(callback, error, result);
            });
        });
    }

    void MySQL::ExecuteSync(const std::string &statement) { _connector.query(statement); }

    void MySQL::Execute(const std::string &statement) {
        _connector.async_query(statement, [this](boost::system::error_code error) {
            if (error) {
                if (error == amy::error::server_gone_error || error == amy::error::server_lost) {
                    // We have lost our connection
                    Reconnect();
                }

                CORE_LOGGING(error) << "Failed to execute statement: " << error.message();
            }
        });
    }

    std::shared_ptr<ResultSet> MySQL::ExecuteQuerySync(const std::string &statement) {
        try {
            auto rs = _connector.query_result(statement);
            return std::make_shared<ResultSet>(rs);
        } catch (const amy::system_error &err) {
            CORE_LOGGING(error) << "Execute query failed: " << err.what();
            return nullptr;
        }
    }

    void MySQL::ExecuteScalar(const std::string &statement,
                              const std::function<void(const MySQLError &error, std::string)> &cb) {
        _connector.async_query_result(statement, [cb](boost::system::error_code error, auto resultSet) {
            if (error) {
                cb(MySQLError(error), nullptr);
                return;
            }

            /*if (resultSet.affected_rows() != 1) {
            }

            cb({}, resultSet[0][0].data());*/
        });
    }
    std::string MySQL::ExecuteScalarSync(const std::string &statement) {
        auto result = _connector.query_result(statement);

        if (result.affected_rows() != 1) {
            throw std::runtime_error("Expected result size of 1");
        }

        return result[0][0].data();
    }

    MySQLError::MySQLError(const boost::system::error_code &error) : _error(error) {}

    MySQLError::MySQLError() {}
}  // namespace core::mysql