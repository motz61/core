// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.

#include "../actions/create_table.hpp"
#include "../structures/column.hpp"
#include "../structures/table.hpp"
#include "_defaults.hpp"
#include "migrations.h"

namespace core {
    namespace mysql {
        namespace migrations {
            M20181027_214100_CreatePlayers::M20181027_214100_CreatePlayers()
                : Migration("M20181027_214100_CreatePlayers") {
                using namespace structures;
                using namespace actions;

                Table::Builder players(GAME_DATABASE, "players");
                players << Column("id", Type("int", 11)
                                            .NotNull()
                                            .PrimaryKey()
                                            .AutoIncrement())
                        << Column("account_id", Type("int", 11).NotNull())
                        << Column("name", Type("varchar", 24).NotNull())
                        << Column("class", Type("tinyint", 2).NotNull())
                        << Column("skill_group", Type("tinyint", 2).NotNull())
                        << Column("x", Type("int", 8).NotNull())
                        << Column("y", Type("int", 8).NotNull())
                        << Column("map_index", Type("tinyint", 4).NotNull());
                Add(new CreateTable(players.operator Table()));
            }
        }  // namespace migrations
    }      // namespace mysql
}  // namespace core
