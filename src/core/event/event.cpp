// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
#include "event.hpp"

#include <utility>

namespace core::event {
    Event::Event(unsigned long id, std::function<int()> function, int timeout)
        : _id(id), _function(std::move(function)), _invocationTime(timeout) {}

    Event::~Event() {}

    bool Event::Execute() {
        _invocationTime = _function();
        return _invocationTime <= 0;
    }

    void Event::Cancel() {}

    bool Event::DecrementTimeout(unsigned int elapsed) {
        _invocationTime -= elapsed;
        return _invocationTime <= 0;
    }
}  // namespace core::event