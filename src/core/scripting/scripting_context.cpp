// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.

#include "scripting_context.hpp"

#include <utility>

namespace core::scripting {
    ScriptingContextAbstract::ScriptingContextAbstract(
        std::shared_ptr<ScriptingEngineAbstract> engine)
        : _engine(std::move(engine)) {}

    ScriptingContextAbstract::~ScriptingContextAbstract() = default;
}  // namespace core::scripting
