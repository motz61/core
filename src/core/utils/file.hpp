// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.

#pragma once

#include <algorithm>
#include <fstream>
#include <iterator>
#include <string>
#include <vector>

namespace core::utils {
    std::vector<uint8_t> ReadFile(const std::string &file);
}  // namespace core::utils
