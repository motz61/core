// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
#pragma once

#include <functional>
#include <memory>
#include <vector>

namespace core::utils {
    template <typename ValueType, typename IDType>
    void QueryList(const std::function<void(IDType, std::function<void(ValueType)>)> &queryFunction,
                   std::vector<IDType> list, const std::function<void(std::shared_ptr<std::vector<ValueType>>)> &cb) {
        std::shared_ptr<std::vector<ValueType>> ret = std::make_shared<std::vector<ValueType>>();

        auto callback = [list, ret, cb, queryFunction](size_t i, auto callback, ValueType res) -> void {
            ret->push_back(res);

            if (i + 1 < list.size()) {
                queryFunction(list[i + 1], [callback, i](ValueType player) { callback(i + 1, callback, player); });
            } else {
                cb(ret);
            }
        };

        queryFunction(list[0], [callback](ValueType player) { callback(0, callback, player); });
    }
}  // namespace core::utils