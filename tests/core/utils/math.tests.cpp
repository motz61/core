// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
#include <core/utils/math.hpp>
#include <gtest/gtest.h>

TEST(Math, CalculateRotationNormalized) {
    ASSERT_FLOAT_EQ(core::utils::Math::CalculateRotation(1, 0), 90);
    ASSERT_FLOAT_EQ(core::utils::Math::CalculateRotation(-1, 0), 270);
    ASSERT_FLOAT_EQ(core::utils::Math::CalculateRotation(0, 1), 0);
    ASSERT_FLOAT_EQ(core::utils::Math::CalculateRotation(0, -1), 180);
    ASSERT_FLOAT_EQ(core::utils::Math::CalculateRotation(0.5, 0.5), 45);
    ASSERT_FLOAT_EQ(core::utils::Math::CalculateRotation(-0.5, 0.5), 315);
}

float RotationBetween(float fromX, float fromY, float toX, float toY) {
    return core::utils::Math::CalculateRotation(toX - fromX, toY - fromY);
}

TEST(Math, CalculateRotation) {
    ASSERT_FLOAT_EQ(RotationBetween(100, 100, 100, 200), 0);
    ASSERT_FLOAT_EQ(RotationBetween(100, 100, 100, 0), 180);
    ASSERT_FLOAT_EQ(RotationBetween(100, 100, 0, 100), 270);
    ASSERT_FLOAT_EQ(RotationBetween(100, 100, 200, 100), 90);
}