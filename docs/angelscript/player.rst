Player
======
A human player object containing all details about a currently online player.

Description
-----------
This class contains all details about the player. Also you can interact and send messages /
packets to the player

Properties
----------
- string name

======== =================
*Setter* ---
*Getter* string get_name()
======== =================

The player name

----

- int8 level

======== ===========================
*Setter* void set_level(uint8 level)
*Getter* uint8 get_level()
======== ===========================

The current level of the player

Methods
-------
- void SendChatMessage(uint8 type, string &in message)

Send a chat message to the player.

.. hint::
    **Type**

    - 0 - normal
    - 1 - system message
    - 3 - group chat
    - 4 - guild chat
    - 6 - shout chat