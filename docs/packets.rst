*.packets
=========

To provide portable and easy to read definition of all packets QuantumCore has
developed an own file format to describe all packets.
The cmake project has a target `compile_packets` which will process all `*.packets`
and generate the related source files.

The standalone compiler can be found under https://gitlab.com/quantum-core/packets-compiler

Syntax
------

The syntax is inspired by C++ and protobuf. 
Every instruction has to end with an semicolon. String values have to been embeded inside quatation marks.

Keywords
--------


namespace
*********

Specify the C++ namespace under which the source files should be generated

target
******

Specify the folder, relative to the packets file, in which the generated files should be placed.

core-directory
**************

Because all packets have a reference to core components you have to provide the 
relative file path to the core root folder.

packet
******

Each packet consist out of the following information:

- Name
- Direction
- Header
- Fields
- Sequence?
- Dynamic?

The following code represents a basic packet:

.. code-block:: none

    packet TestPacket(->0x01) {
        uint32 test;
        uint8 test2[4];
        string(17) str;
    }

A packet definition starts with the keyword `packet` followed by the name of the packet,
in this case `TestPacket`. Following an open bracket, than the direction arrow and than the header
in hex representation. After that a closing bracket and than an opening brace.

.. note::

    The following directions are available:

    - `->` Incoming
    - `<-` Outgoing
    - `<->` Bidirectional

Now the list of fields are following. Every field consist of a type, a name and if it's an array by the length of the array.

.. note::

    The following types are available:

    - uint8
    - int8
    - uint16
    - int16
    - uint32
    - int32
    - float
    - string(length)
    - raw(length)

    Some types need to specify the length of the type. It is also possible to use custom defined types.

After listing all fields it is possible to define to boolean flags at the packet.

- `sequence` - Enable sequencing on this packet, the client sends an additional byte at the end of the packet to verify the version of the client.
- `dynamic` - Enable dynamic sized data, this will automatically add a size field to the beginning of the packet.

type
****

It is possible to define reusable types. 
Types are defined exactly the same as packets are except it doesn't have the brackets for the direction and the header.

.. note::

    Currently types are only allowed inside a packet.